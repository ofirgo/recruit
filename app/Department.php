<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Department extends Model
{
    // protected $fillable = [
    //     'departments',
    // ];

    public function users(){
        return $this->hasMany('App\User');
    }
}
