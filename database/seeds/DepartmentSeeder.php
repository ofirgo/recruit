<?php

use Illuminate\Database\Seeder;
use carbon\carbon;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            DB::table('departments')->insert([
                [
                'departments' => 'HR',
                'created_at'=> date('Y-m-d G:i:s'),
                'updated_at'=> date('Y-m-d G:i:s'),
                ],
                [
                    'departments' => 'Management',
                    'created_at'=> date('Y-m-d G:i:s'),
                    'updated_at'=> date('Y-m-d G:i:s'),
                ],
        
            ]);
        }
    }
}
