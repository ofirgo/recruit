<?php

use Illuminate\Database\Seeder;
#use Illuminate\Database\Eloquent;


class CandidatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('candidates')->insert([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'created_at'=> date('Y-m-d G:i:s'),
            'updated_at'=> date('Y-m-d G:i:s'),
        ]);
    }

}



