@extends('layouts.app')

@section('content')

        <h1>Edit candidate</h1>
        <form method="post" action="{{action('CandidatesController@update',$candidate->id)}}"> 
        @csrf
        @method('PATCH')
        <div class="form-group">
            <lable for= "name">Candidate name</lable>
            <input type="text" class="form-conttrol" name="name" value={{$candidate->name}}>
            
        </div>
        <div  class="form-group"> 
            <lable for= "email">Candidate email</lable>
            <input type="text" class="form-conttrol" name="email"  value={{$candidate->email}}>
        </div>
        <div>
            <input type="submit" name="submit" value="Update candidate">
        </div>
        </form>         

       
@endsection

