@extends('layouts.app')

@section('content')
           
        <h1>Create candidate</h1>
        <form method="post" action="{{action('CandidatesController@store')}}">
        @csrf
        <div>
            <lable for= "name">Candidate name</lable>
            <input  type="text" name="name">
        </div>
        <div>
            <lable for= "email">Candidate email</lable>
            <input type="text" name="email">
        </div>
        <div>
            <input type="submit" name="submit" value="Create candidate">
        </div>
        </form>         

       
 @endsection
